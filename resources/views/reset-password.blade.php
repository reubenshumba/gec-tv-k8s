@extends("layouts.app")

@section("content")
    <div class="justify-content-center ">
        <div class="row ">

            <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                <div class="row justify-content-center card-body ">
                    <div class="col-md-8">
                        <div class="mb-4 text-center">
                            <img src="{{asset('images/GEC-logo.png')}}" width="90"/>
{{--                            <h3 class="mt-1" >Gec Equip</h3>--}}
                            <p class="mb-4 h5">Reset Password </p>
                        </div>
                        <form action="{{route('reset-my-password')}}" method="post" class="font-weight-bold" >
                            @csrf
                            <div class="form-group first mb-4">
                                <label for="username">Account Email</label>
                                <input type="text" required name="email"  value="{{old('email')}}"
                                       class="form-control @error('email') is-invalid @enderror" id="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group last mb-4">
                                <label for="password">Password</label>
                                <input type="password" required  name="password" class="form-control @error('password') is-invalid @enderror" id="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group last mb-4">
                                <label for="confirmPassword">Confirm Password</label>
                                <input autocomplete="current-password" type="password" name="password_confirmation"
                                       required class="form-control  @error('password') is-invalid @enderror"
                                       id="confirmPassword">

                            </div>

                            <input type="submit" value="Update" class="btn btn-block btn-outline-info form-control ">

                            <div class="form-group last mt-1">
                            </div>

                            <span class="d-block text-left my-4 text-muted">&mdash; Sign In or Sign up &mdash;</span>

                            <div class="d-flex justify-content-evenly ">
                                <a href="{{route('sign-in')}}" class="btn btn-outline-primary">
                                    <span class="fa fa-sign-in mr-3">  Sign In </span>
                                </a>
                                <a href="{{route('sign-up')}}" class="btn btn-outline-dark">
                                    <span class="fa fa-sign-in mr-3"> Sign Up</span>
                                </a>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-md-6 shadow-lg justify-content-center p-5">
                <img  src="{{asset('images/bg.jpg')}}" alt="Image" class="img-fluid" width="800px">
            </div>
        </div>
    </div>
@endsection




