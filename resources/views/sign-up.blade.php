@extends("layouts.app")

@section("content")
    <div class="justify-content-center ">
        <form action="{{route('sign-up')}}" method="post" class="font-weight-bold">
            @csrf

            <div class="row ">

                <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                    <div class="row justify-content-center card-body ">
                        <div class="col-md-8">
                            <div class="mb-4 text-center">
                                <img src="{{asset('images/GEC-logo.png')}}" width="90"/>
                                <h3 class="mt-1"></h3>
                                <p class="mb-4 h5">Sign Up </p>
                            </div>

                            <div class="form-group  mb-3 ">
                                <label for="title">Select Title
                                </label>
                                <select id="title" name="title"  class="form-control  @error('title') is-invalid @enderror">
                                    <option value="Pastor">Pastor</option>
                                    <option value="Reverend">Reverend</option>
                                    <option value="H.E">H.E</option>
                                    <option value="Prophet">Prophet</option>
                                    <option value="Apostle">Apostle</option>
                                    <option value="Evangelist">Evangelist</option>
                                    <option value="Sir">Sir</option>
                                    <option value="Madam">Madam</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Deacon">Deacon</option>
                                    <option value="Deaconess">Deaconess</option>
                                    <option value="Director">Director</option>
                                </select>

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="form-group first mb-4">
                                <label for="firstName">First Name</label>
                                <input type="text" name="firstName" value="{{old('firstName')}}" required class="form-control  @error('firstName') is-invalid @enderror" id="firstName">
                                @error('firstName')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group first mb-4">
                                <label for="lastName">Last Name</label>
                                <input type="text" name="lastName" value="{{old('lastName')}}"  required class="form-control  @error('lastName') is-invalid @enderror" id="lastName">
                                @error('lastName')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>


                            <div class="form-group last mb-4">
                                <label for="password">Password</label>
                                <input autocomplete="current-password" type="password" name="password" required
                                       class="form-control  @error('password') is-invalid @enderror" id="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group last mb-4">
                                <label for="confirmPassword">Confirm Password</label>
                                <input autocomplete="current-password" type="password" name="password_confirmation"
                                       required class="form-control  @error('password') is-invalid @enderror"
                                       id="confirmPassword">

                            </div>

                        </div>
                    </div>

                </div>

                {{-- the other side --}}

                <div class="col-md-6 shadow-lg justify-content-center p-5">
                    <p class="mb-4 h5">Other details </p>

                    <div class="form-group first mb-4">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{old('email')}}"  class="form-control  @error('email') is-invalid @enderror" id="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group mb-2">
                        <label class="text-left" for="gender">Select Gender</label>
                        <select class="form-control @error('gender') is-invalid @enderror" id="gender" name="gender">
                            <option value="-1"> . . .</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                        @error('gender')
                        <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="form-group  mb-3">

                        <label for="member">Are you a member of Gospel Envoy's church ? </label>
                        <select id="member" name="member"
                                class="form-control  @error('member') is-invalid @enderror">

                            <option value="false">No I'm not </option>
                            <option value="true">Yes I'm</option>
                        </select>

                        @error('member')
                        <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                        @enderror

                    </div>




                    <div class="d-flex justify-content-evenly ">

                        <div class="text-info">
                            <p class="checkbox-wrap checkbox-primary text-left  text-muted "
                               style="font-size: 10px; letter-spacing:  0.0625em; ">
                                By signing up, you confirm that you accept not share your login details with a third
                                party and that the ministry retains full and exclusive copyright ownership of all
                                segments of this service hence no segment of the broadcast should be recorded, copied,
                                re-transmitted in any form or by any means, stored in any form of electronic retrieval
                                system and used for any other purpose than that which it was intended.

                            </p>
                        </div>
                    </div>

                    <input type="submit" value="Sign Up" class="btn btn-block btn-outline-info form-control shadow ">

                    <span class="d-block text-center my-4 text-muted">&mdash;  or &mdash;</span>
                    <div class="mb-2">
                        <a href="{{route('sign-in')}}" class="text-dark h4 mr-3 shadow-lg  p-2">
                            <span class="fa fa-sign-in mr-3  ">login</span>
                        </a>
                    </div>

                </div>

                {{--End of the other side --}}
            </div>


        </form>
    </div>
@endsection




