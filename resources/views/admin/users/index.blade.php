@extends("layouts.app")
@section("content")
    <div class="row text-center h-100 justify-content-center align-items-center">

        <div class="col-md-12 col-lg-12 m-5">
            <div class="text-center text-info">
                <h2 class="heading-section text-left pt-10">GEC LIVE | ADMIN PORTAL</h2>
            </div>
            <div class="p-0">
                <div><h5 class="mb-4 text-text text-info">Members</h5></div>
                <div class="social d-flex text-center">
                    <a class="px-2 py-2 mr-md-1 rounded btn btn-outline-info"
                       href="{{ route("admin-members-create") }}"><span
                            class="ion-logo-facebook mr-2"></span>Add Member</a>

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                        <span class="px-2 py-2 mr-md-1 rounded btn btn-outline-danger"> Logout <span>
                    </a>

                    <form id="frm-logout" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="table-responsive">
                    {{ $members->links() }}
                    <table class="table table-hover">
                        <thead>
                        <tr class="text-info text-left">
                            <th scope="col">#</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Active</th>
                            <th scope="col">Enable</th>
                            <th scope="col">Authority</th>
                            <th scope="col">Branch ID</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($members as $key => $member)
                            <tr class="text-left">
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{ $member->username }}</td>
                                <td>{{ $member->email }}</td>
                                <td>{{ $member->first_name }}</td>
                                <td>{{ $member->last_name }}</td>
                                <td>{{ $member->active ? 'Yes' : 'No' }}</td>
                                <td>{{ $member->enable ? 'Yes' : 'No' }}</td>
                                <td>{{ $member->authority_authorityid }}</td>
                                <td>{{ $member->branchid }}</td>
                                <td>
                                    <a class="btn btn-outline-success btn-sm"
                                       href="{{ route("admin-members-edit", ['id' => $member->memberid]) }}">Edit</a>

                                    <button class="btn btn-outline-danger btn-sm btn-sm"
                                            data-toggle="modal"
                                            data-target="{{ '#delete'.$member->memberid }}"
                                            type="button">
                                        Delete
                                    </button>

                                    <!-- Modal -->
                                    <div aria-hidden="true" class="modal fade" role="dialog"
                                         tabindex="-1"
                                         aria-labelledby="{{ 'delete'.$member->memberid.'-'.$member->memberid }}"
                                         id="{{ 'delete'.$member->memberid }}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header text-danger">
                                                    <h5 class="modal-title"
                                                        id="{{ 'delete'.$member->memberid.'-'.$member->memberid }}"
                                                    >Confirmed Deletion</h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal"
                                                            type="button">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-danger">Are you sure you want to delete
                                                    <h5>{{ $member->first_name.' '.$member->last_name }} </h5></p>
                                                </div>
                                                <form  action="{{ route("admin-members-delete", ['id' => $member->memberid]) }}" method="post">
                                                    @csrf
                                                    <button class="btn btn-outline-danger" type="submit">
                                                        Confirm Delete
                                                    </button>
                                                </form>
                                                <div class="modal-footer">
                                                    <p class="text-left ">{{ $member->description }}</p>
                                                    <button class="btn btn-secondary" data-dismiss="modal"
                                                            type="button">
                                                        Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $members->links() }}
                </div>

                <div class="social d-flex text-center">
                    <a class="px-2 py-2 mr-md-1 rounded btn btn-outline-info"
                       href="{{ route("admin-members-create") }}"><span
                            class="ion-logo-facebook mr-2"></span>Add Member</a>
                </div>
            </div>
        </div>
    </div>
@endsection
