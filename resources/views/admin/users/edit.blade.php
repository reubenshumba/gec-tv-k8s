@extends("layouts.app")

@section("content")
    <section class="container h-100 text-info">
        <form class="signin-form text-left " action="{{route("admin-meetings-update",['id'=>$meeting->meetingid])}}" method="post">
            @csrf
            <div class="h-20 justify-content-around align-items-center  ">
                <h2 class="heading-section text-left text-capitalize mt-5 "> Meeting</h2>
                <a class="btn btn-outline-info btn-sm" href="{{route("admin-meetings")}}"
                >{{'< View All Meetings'}} </a>
                <h5 class="mb-4 text-center">Edit meeting </h5>
            </div>

            <div class="row  text-left h-100 justify-content-center align-items-center font-weight-bold">
<input type="hidden" value="{{$meeting->meetingid}}" name="meetingid">
                <div class="col-md-6 col-lg-6  p-5">
                    <div class="login-wrap p-0 text-left">

                        <div class="form-group  @error('host') is-invalid @enderror  mb-3">
                            <label class="text-left" for="host">Speaker </label>
                            <input id="host" class="form-control" placeholder="Speaker e.g Pastor Choolwe"
                                   required type="text" name="host" value="{{old("host",$meeting->host)}}">

                            @error('host')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  @error('name') is-invalid @enderror  mb-3">
                            <label class="text-left" for="name">Tag Name </label>
                            <input id="name" class="form-control" placeholder="Title e.g Sunday Service"
                                   required name="name" type="text" value="{{old('name',$meeting->name)}}">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  @error('description') is-invalid @enderror  mb-3">
                            <label class="text-left" for="description">Short Description </label>
                            <input id="description" class="form-control" placeholder="Short Description "
                                   name="description" type="text" value="{{old('description',$meeting->description)}}">
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  @error('meeting_url') is-invalid @enderror  mb-3 ">
                            <label class="text-left" for="meeting_url">Enter URL or Embed Code </label>
                            <textarea id="meeting_url" class="form-control" placeholder="Url or Embed "
                                      required name="meeting_url" type="text" value="{{old('meeting_url',$meeting->meeting_url)}}"
                            >{{old('meeting_url',$meeting->meeting_url)}}</textarea>
                            @error('meeting_url')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  mb-3">
                            <label class="text-left" for="chat">Chat Embed Code </label>
                            <textarea id="for" class="form-control" placeholder="Chat Embed Code "
                                      name="meeting_chat_url" type="text"
                                      value="{{old('meeting_chat_url',$meeting->meeting_chat_url)}}"
                            >{{old('meeting_chat_url',$meeting->meeting_chat_url)}}</textarea>

                        </div>

                    </div>
                </div>
                <div class="col-md-6 text-left p-5 pt-0">
                    <div class="login-wrap p-0 text-left">
                        <div class="form-group  @error('enable') is-invalid @enderror  mb-3 ">
                            <label class="text-left" for="enabled">Make the link accessible to views </label>
                            <select class="form-control" id="enabled" name="enable">
                                <option value="1" @if($meeting->enable) selected @endif >Enabled to Audience</option>
                                <option value="0" @if(!$meeting->enable) selected @endif >Disable to Audience</option>
                            </select>
                            @error('enable')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  @error('active') is-invalid @enderror  mb-3 ">
                            <label class="text-left" for="active">Publish or Save</label>
                            <select class="form-control" id="active" name="active">
                                <option value="1" @if($meeting->active) selected @endif>Public to Audience</option>
                                <option value="0"@if(!$meeting->active) selected @endif>Save but dont publish to Audience</option>
                            </select>
                            @error('active')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  @error('authority') is-invalid @enderror  mb-3 ">
                            <label class="text-left" for="authority">Select
                                Audience</label>
                            <select  class="form-control" id="authority"  multiple="multiple" name="authority[]">

                                @foreach($authorities as $authority)
                                    <option value="{{$authority->authorityid}}" @foreach($meeting->authorities as $meetingAuthority) @if($authority->authority_authorityid == $meetingAuthority ) selected @endif @endforeach >{{$authority->authority_name}}</option>
                                @endforeach
                            </select>

                            @error('authority')
                            <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group  mt-5">
                            <button class="form-control btn btn-info submit px-3" type="submit">Update Meeting
                            </button>
                        </div>

                    </div>
                </div>


            </div>


        </form>
    </section>

@endsection
