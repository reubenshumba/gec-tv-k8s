@extends("layouts.app")
@section("content")
    <div class="row  text-center h-100 justify-content-center align-items-center">

        <div class="col-md-12 col-lg-12 m-5">
            <div class=" text-center text-info">
                <h2 class="heading-section text-left pt-10">GEC LIVE | ADMIN PORTAL</h2>
            </div>
            <div class=" p-0 ">
                <div><h5 class="mb-4 text-text text-info">Meetings</h5></div>
                <div class="social d-flex text-center">
                    <a class="px-2 py-2 mr-md-1 rounded btn btn-outline-info"
                       href="{{route("admin-meetings-create")}}"><span
                            class="ion-logo-facebook mr-2"></span>Add Meeting</a>

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                    <span class="px-2 py-2 mr-md-1 rounded btn btn-outline-danger"> Logout </span>
                    </a>

                    <form id="frm-logout" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="table-responsive">
                    {{ $meetings->links() }}
                    <table class="table table-hover">
                        <thead>
                        <tr class="text-info text-left">
                            <th scope="col">#</th>
                            <th scope="col">ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Host</th>
                            <th scope="col">video information</th>
                            <th scope="col">Access Enabled</th>
                            <th scope="col">Visibility</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($meetings as $key => $value)
                            <tr class="text-left">
                                <th scope="row">{{$key+1}}</th>
                                <th scope="row">{{$value->meetingid}}</th>
                                <td>{{$value->name}}</td>
                                <td>{{$value->host}}</td>
                                <td class="">

                                    <button class="btn btn-outline-info btn-sm btn-sm"
                                            data-toggle="modal"
                                            data-target="{{'#'.$value->meetingid}}"
                                            type="button">
                                        View
                                    </button>

                                    <!-- Modal -->
                                    <div aria-hidden="true" class="modal fade" role="dialog"
                                         tabindex="-1"
                                         aria-labelledby="{{$value->meetingid.'-'.$value->meetingid}}"
                                         id="{{$value->meetingid}}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header text-info">
                                                    <h5 class="modal-title"
                                                        id="{{$value->meetingid."-".$value->meetingid}}">
                                                        {{$value->name." with ".$value->host}}</h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal"
                                                            type="button">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    @if($value->embed_supported_type =='URL')

                                                        <div
                                                            style="position:relative;padding-bottom:56.25%;overflow:hidden;height:0;max-width:100%;">
                                                            <iframe id="90b03aa3-c92d-8017-6899-6b0c3575c7c0"
                                                                    src="{{html_entity_decode($value->meeting_url)}}"
                                                                    width="100%" height="100%" frameborder="0"
                                                                    scrolling="no" allow="autoplay;encrypted-media"
                                                                    allowfullscreen webkitallowfullscreen
                                                                    mozallowfullscreen oallowfullscreen
                                                                    msallowfullscreen
                                                                    style="position:absolute;top:0;left:0;"></iframe>
                                                        </div>

                                                    @endif

                                                    @if($value->embed_supported_type =='IFRAME')
                                                        {{html_entity_decode($value->meeting_url)}}
                                                    @endif

                                                </div>
                                                <div class="modal-footer">

                                                    <p class="text-left ">{{$value->description}}</p>
                                                    <button class="btn btn-secondary" data-dismiss="modal"
                                                            type="button">
                                                        Close
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                                <td>
                                    @if($value->enable)
                                        <span class="text-success">{{'Visible to Audience'}}</span>
                                    @else
                                        <span class="text-warning">{{'Not Visible'}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if($value->active)
                                        <span class="text-success">{{'Published'}}</span>
                                    @else
                                        <span class="text-warning">{{'Saved but not Published'}}</span>
                                    @endif
                                </td>
                                <td><a class="btn btn-outline-success btn-sm"
                                       href="{{route("admin-meetings-edit",['id'=>$value->meetingid])}}"> Edit</a>

                                    <button class="btn btn-outline-danger btn-sm btn-sm"
                                            data-toggle="modal"
                                            data-target="{{'#delete'.$value->meetingid}}"
                                            type="button">
                                        Delete
                                    </button>

                                    <!-- Modal -->
                                    <div aria-hidden="true" class="modal fade" role="dialog"
                                         tabindex="-1"
                                         aria-labelledby="{{'delete'.$value->meetingid.'-'.$value->meetingid}}"
                                         id="{{'delete'.$value->meetingid}}">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header text-danger">
                                                    <h5 class="modal-title"
                                                        id="{{'delete'.$value->meetingid.'-'.$value->meetingid}}"
                                                    >Confirmed Deletion</h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal"
                                                            type="button">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-danger">Are you sure you want to delete
                                                    <h5>{{$value->name}} </h5></p>
                                                </div>
                                                <form  action="{{route("admin-meetings-delete",['id'=>$value->meetingid])}}" method="post"
                                                    >
                                                    @csrf
                                                    <button class="btn btn-outline-danger" type="submit">
                                                        Confirm Delete
                                                    </button>
                                                </form>
                                                <div class="modal-footer">

                                                    <p class="text-left ">{{$value->description}}</p>
                                                    <button class="btn btn-secondary" data-dismiss="modal"
                                                            type="button">
                                                        Close
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $meetings->links() }}
                </div>

                <div class="social d-flex text-center">
                    <a class="px-2 py-2 mr-md-1 rounded btn btn-outline-info"
                       href="{{route("admin-meetings-create")}}"><span
                            class="ion-logo-facebook mr-2"></span>Add Meeting</a>

                </div>
            </div>
        </div>

    </div>

@endsection
