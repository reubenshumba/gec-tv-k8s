@extends("layouts.app")

@section("content")
    <div class="justify-content-center ">
        <div class="row ">

            <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                <div class="row justify-content-center card-body ">
                    <div class="col-md-8">
                        <div class="mb-4 text-center">
                            <img src="{{asset('images/GEC-logo.png')}}" width="90"/>
{{--                            <h3 class="mt-1" >Gec Equip</h3>--}}
                            <p class="mb-4 h5">Sign In </p>
                        </div>
                        <form action="{{route('sign-in')}}" method="post" class="font-weight-bold" >
                            @csrf
                            <div class="form-group first mb-4">
                                <label for="username">GEC User ID or Email</label>
                                <input type="text" required name="username"  value="{{old('username')}}"
                                       class="form-control @error('username') is-invalid @enderror" id="username">

                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group last mb-4">
                                <label for="password">Password</label>
                                <input type="password" required  name="password" class="form-control @error('password') is-invalid @enderror" id="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group  mb-3">
                                <label for="meeting">Select Service
                                </label>
                                <select id="meeting" required name="meeting" class="form-control @error('meeting') is-invalid @enderror">
                                    @if(count($meetings) > 1)
                                        <option value="-1">Select Meeting</option>
                                    @endif
                                    @foreach($meetings as $meeting)
                                        <option value="{{$meeting->meetingid}}" >{{$meeting->name}}</option>
                                    @endforeach

                                </select>
                                @error('meeting')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="d-flex mb-5 align-items-center">
                                <label class="control control--checkbox mb-0"><span class="caption">Remember me</span>
                                    <input type="checkbox" name="remember" checked="checked"/>
                                    <div class="control__indicator"></div>
                                </label>

                            </div>



                            <input type="submit" value="Log In" class="btn btn-block btn-outline-info form-control ">

                            <div class="form-group last mt-1">
{{--                                <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span>--}}
                            </div>

                            <span class="d-block text-left my-4 text-muted">&mdash; Reset Password or Sign up &mdash;</span>

                            <div class="d-flex justify-content-evenly ">
                                <a href="{{route('reset-my-password')}}" class="btn btn-outline-primary">
                                    <span class="fa fa-sign-in mr-3">  Reset Password</span>
                                </a>
                                <a href="{{route('sign-up')}}" class="btn btn-outline-dark">
                                    <span class="fa fa-sign-in mr-3"> Sign Up</span>
                                </a>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-md-6 shadow-lg justify-content-center p-5">
                <img  src="{{asset('images/bg.jpg')}}" alt="Image" class="img-fluid" width="800px">
            </div>
        </div>
    </div>
@endsection



