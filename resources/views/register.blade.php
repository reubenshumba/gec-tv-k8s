@extends("layouts.app")

@section("content")
    <div class="justify-content-center ">
        <form action="{{route('register',['id'=>$member->memberid])}}" method="post" class="font-weight-bold">
            @csrf

            <div class="row ">

                <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                    <div class="row justify-content-center card-body ">
                        <div class="col-md-8">
                            <div class="mb-4 text-center">
                                <img src="{{asset('images/GEC-logo.png')}}" width="90"/>
                                <h3 class="mt-1"> Almost done</h3>
                                <p class="mb-4 h5">Fill in your Ministry Information</p>
                            </div>
                            <input class="form-control" placeholder="memberID" required
                                   hidden="hidden" name="memberid"
                                   type="hidden" value="{{$member->memberid}}">

                            <div class="form-group  mb-3">

                                <label for="foundationSchool">Have you completed foundation School
                                </label>
                                <select id="foundationSchool" name="foundationSchool"
                                        class="form-control  @error('foundationSchool') is-invalid @enderror">

                                    <option value="Yes">Yes, I have</option>
                                    <option value="Not">No, I have not</option>
                                    <option value="Studen">No, But enrolled</option>
                                </select>

                                @error('foundationSchool')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="form-group  mb-3">

                                <label for="branch">Select Branch </label>
                                <select id="branch" name="branch"
                                        class="form-control  @error('branch') is-invalid @enderror">

                                    <option value="-1">. . . </option>
                                   @foreach($branches as $branch)
                                        <option value="{{$branch->branchid}}">{{$branch->branch_name}}</option>
                                    @endforeach
                                </select>

                                @error('branch')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="form-group  mb-3">

                                <label for="department">Select Department
                                </label>
                                <select id="department" name="department"
                                        class="form-control  @error('department') is-invalid @enderror">

                                    <option value="-1">. . .</option>
                                    @foreach($departments as $department)
                                        <option value="{{$department->departmentid}}">{{$department->department_name}}</option>
                                    @endforeach
                                </select>

                                @error('department')
                                <span class="invalid-feedback" role="alert">
                                    <strong>  {{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <input type="submit" value="Save" class="btn btn-block btn-outline-info form-control shadow">

                        </div>
                    </div>

                </div>

                {{-- the other side --}}

                <div class="col-md-6 shadow-lg justify-content-center p-5">
                    <div class="shadow-lg">
                        <img src="{{asset('/images/bg.jpg')}}" alt="Image" class="img-fluid" width="800px">
                    </div>
                </div>

                {{--End of the other side --}}
            </div>


        </form>
    </div>
@endsection




