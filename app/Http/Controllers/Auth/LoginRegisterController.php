<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\AuthorityMeeting;
use App\Models\AuthorityMember;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Meeting;
use App\Models\Member;
use App\Models\Guest;
use App\Models\Follower;
use App\Notifications\EmailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
//use PragmaRX\Countries\Package\Countries;


class LoginRegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        Log::info('inside the constructor.');
    }



    public function guest()
    {
        // $countries = new Countries();
        // $countriesList = $countries->all()->pluck('name.common');
        return view("index", ["countriesList"=>1]);
    }

    public function guestStore(Request $request)
    {
        // Validate the request data
        $validatedData = $request->validate([
            'fullname' => 'required|string|max:250',
            'email' => 'required|email',
            'participant' => 'required|integer|min:0',
            'phoneNumber' => 'required|string|min:10|max:20',
            //'city' => 'required|string|max:100',    // Add validation for 'city'
            'country' => 'required|string|max:100', // Add validation for 'country'
            'member' => 'required|boolean',         // Add validation for 'member'
        ]);

        // Check if guest exists by fullname and email
        $guest = Guest::where('fullname', $validatedData['fullname'])
            ->where('email', $validatedData['email'])
            ->first();

        if ($guest) {
            // Update participant if it's greater than the existing value
            if ($validatedData['participant'] > $guest->participant) {
                $guest->participant = $validatedData['participant'];
            }
            // Update the new fields
            $guest->city = $validatedData['country'];
            $guest->country = $validatedData['country'];
            $guest->member = $validatedData['member'];

            $guest->save();
        } else {
            // Create a new guest record
            $guest = new Guest();
            $guest->fullname = $validatedData['fullname'];
            $guest->email = $validatedData['email'];
            $guest->participant = $validatedData['participant'];
            $guest->phoneNumber = $validatedData['phoneNumber'];
            $guest->meetingid = 1;

            // Set the new fields
            $guest->city = $validatedData['country'];
            $guest->country = $validatedData['country'];
            $guest->member = $validatedData['member'];

            $guest->save();
        }

        $meeting = Meeting::findOrFail(1);

        // Redirect to the meeting or any other desired location
        return redirect()->route("live-service", ["id" => 1])->with("success", "Welcome " . $guest->fullname);
    }



    public function index()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->orderBy('meetingid', 'DESC')
            ->get("*");
        return view("index", ["meetings" => $meetings]);
    }
    public function tc()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->where("meetingid", 20)
            ->orderBy('meetingid', 'DESC')
            ->get();
        return view("index", ["meetings" => $meetings]);
    }
    public function cc()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->where("meetingid",20)
            ->orderBy('meetingid', 'DESC')
            ->get();
        return view("index", ["meetings" => $meetings]);
    }
    public function fl()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->where("meetingid", 17)
            ->orderBy('meetingid', 'DESC')
            ->get();
        return view("index", ["meetings" => $meetings]);
    }
    public function fsl()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->where("meetingid", 17)
            ->orderBy('meetingid', 'DESC')
            ->get();
        return view("index", ["meetings" => $meetings]);
    }
    public function cm()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->where("meetingid", 12)
            ->orderBy('meetingid', 'DESC')
            ->get();
        return view("index", ["meetings" => $meetings]);
    }
    public function clm()
    {
        $meetings = DB::table('meetings')
            ->where("active", 1)
            ->where("meetingid", 12)
            ->orderBy('meetingid', 'DESC')
            ->get();
        return view("index", ["meetings" => $meetings]);
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect()->route("login");
    }

    public function login(Request $request)
    {
        // Validate details
        Log::info('Validation.');
        $this->validate($request, [
            'meeting' => 'required|not_in:-1',
            'password' => 'required|min:6',
            'username' => 'required|min:3',
        ]);

        // Find the member by username or email
        $member = Member::whereRaw('username = ? or email = ?', [$request->username, $request->username])->first();

        if (!$member || !auth()->attempt([
                'username' => $member->username,
                'password' => $request->password,
                'enable' => 1,
                'active' => 1
            ])) {
            Log::info('Invalid credentials.');
            return back()->with('danger', 'Invalid Credentials.');
        }

        // Get authorities for the given meeting
        //$authorities = Meeting::find($request->meeting)->authorities;
        $meeting = Meeting::find($request->meeting);
        if (!$meeting) {
            // Handle the case where the meeting is not found
            return back()->with('danger', 'Meeting not found');
        }

        // Get authorities associated with the member
        $memberAuthorities = $member->authorities()->select('authorities.authorityid')->pluck('authorityid')->toArray();
        $authorities = collect($meeting->authorities()->select('authorities.authorityid')->pluck('authorityid')->toArray());


        // Check if the member has access to the meeting
        $hasAccess = $authorities->contains(function ($authority) use ($member, $memberAuthorities) {
            return $member->authority_authorityid == $authority || in_array($authority, $memberAuthorities);
        });


        if ($hasAccess) {
            Log::info('Logging successful.');
            $this->registerFollower($member->username, $member->username, $request->meeting);
            return redirect()->route('stream', ['id' => $request->meeting])->with('success', 'Welcome ' .
                $member->title . ' ' . $member->first_name . ' ' . $member->last_name);
        }

        Log::info($member->authority_authorityid . ' == ' . $authorities->pluck('authorityid')->implode(','));

        return back()->with('danger', 'You don\'t have access to this meeting');
    }


    public function signUp()
    {
        return view("sign-up");
    }

    public function saveSignUp(Request $request)
    {
        //validate details
        $this->validate($request, [
            "title" => "required|not_in:-1",
            "firstName" => "required|min:3|max:255",
            "lastName" => "required|min:3|max:255",
            "password" => "required|confirmed|min:6|max:255",
            "email" => "required|email|max:255,unique:members,email",
            "gender" => "required|not_in:-1",
            'member' => "required|in:true,false"

        ]);

        if (Member::where('email', '=', $request->email)->exists()) {
            return back()->with("danger", " Oops!, Email already in use");
        }

        Log::info('About to sign up.');
        $lastUser = DB::table('members')->orderBy('memberid', 'DESC')->first();
        $name = isset($lastUser) && $lastUser != null ? $lastUser->memberid + 1 : 1;

        if ($request->member == "true") {
            $name = env('MEMBER_PREFIX_ID') . $name;

        } else {
            $name = env('NON_MEMBER_PREFIX_ID') . $name;
        }

        //save
        // $authority = DB::table('authorities')->where('authorityid',  env('NON_MEMBER_AUTHORITY_ID'))->first();
        $user = Member::create(
            [
                "email" => $request->email,
                "password" => Hash::make($request->password),
                'username' => $name,
                'first_name' => $request->firstName,
                'last_name' => $request->lastName,
                'active' => true,
                'date_created' => date('Y-m-d H:i:s'),
                'date_updated' => date('Y-m-d H:i:s'),
                'enable' => true,
                'foundation_school' => "Not",
                'gender' => $request->gender,
                'title' => $request->title,
                'authority_authorityid' =>env('NON_MEMBER_AUTHORITY_ID'),
                'branchid' => 1
            ]
        );
        $user->save();
        Log::info('Saved successful.');
        $appName =env('APP_NAME', 'GEC TV');
        $appUrl =env('APP_URL', 'tv.gecequip.org');
        $email = [
            'greeting' => 'Greeting '.$request->title." ".$request->firstName." ".$request->lastName.',',
            'body' => "You have successful Registered on $appName, your GEC unique USER ID: ".$user->username,
            'thanks' => 'This is from '.$appUrl,
            'actionText' => 'Login',
            'actionURL' => url('/'),
            'id' => $user->memberid
        ];

        Notification::send($user, new EmailNotification($email));

        $message = 'Registration Successful. Your User ID is ' . $user->username;
        //redirect: sign up or proceed to other details
        if ($request->member  == "true") {
            $userArray =json_decode($user,1);
            $id = array_key_exists("memberid",$userArray) ? $user->memberid : $user->id;
            Log::info('route to register: '.$id);
            return redirect()->route("register", ["id" => $id ])->with('success', $message);
        } else {
            Log::info('route to sign-in');
            return redirect()->route("sign-in")->with('success', $message);
        }
    }

    public function register($user)
    {
        if ($user == null || $user <= 0) {
            dd("Invalid request");
        }
        $branches = Branch::select()->where("active", "1")->get("*");
        $departments = Department::select()->where("active", "1")->get("*");
        $member = DB::table('members')->where('memberid', $user)->first();
        Log::info('inside Register: '.json_encode($branches).' '.json_encode($member));
        return view("register", ["branches" => $branches, "departments" => $departments, "member" => $member]);

    }

    public function saveRegister(Request $request, $id)
    {
        Log::info('inside Register: '.$id);
        $this->validate($request, [
            "branch" => "required|not_in:-1",
            "department" => "required|not_in:-1",
            "memberid" => "required",
            "foundationSchool" => "required|not_in:-1"
        ]);
        Log::info('branch : '.$request->branch);

        $user = DB::update("update members set branchid = ? , foundation_school = ?,authority_authorityid=?  where memberid = ?",
            [$request->branch, $request->foundationSchool, env('MEMBER_AUTHORITY_ID') ,$request->memberid]);
        Log::info('save Register: '.json_encode($user));
        if ($user) {
            $user = DB::table('members')->where('memberid', $request->memberid)->first();
            Log::info("You have successful registered, Gec User ID is " . $user->username);
            return redirect()->route("sign-in")->with("success", "Registration Successful. Your User ID is" . $user->username);
        } else {
            Log::info('Oops!, could not register Gec Member: '.$id);
            return back()->with("danger", " Oops!, could not register Gec Member");
        }

    }

    public function registerFollower($email,$username,$meetingID){
        $follower =  Follower::create(
            [
                "email" =>$email,
                'username' => $username,
                'meetingID' => $meetingID,
                'date_created' => date('Y-m-d H:i:s'),
                'date_updated' => date('Y-m-d H:i:s'),
            ]
        );
        $follower->save();
    }


    public function updatePassword(Request $request)
    {
        //validate details
        $this->validate($request, [
            "password" => "required|confirmed|min:6|max:255",
            "email" => "required|email|max:255,unique:members,email",
        ]);

        if (!Member::where('email', '=', $request->email)->exists()) {
            return back()->with("danger", " Oops!, Failed to update password");
        }


//        $user =  DB::table('members')
//            ->where('email', $request->email)
//            ->update(['password' => Hash::make($request->password)]);

//        $user = Member::where('email', $request->email)
//            ->update(['password' => Hash::make($request->password)]);

        $user = DB::update("update members set password = ?  where email = ?",
            [ Hash::make($request->password) ,$request->email]);

        if ($user) {
            // $user = DB::table('members')->where('email', $request->email)->first();
            $user = Member::whereRaw("email = ?", [$request->email])->first();

            Log::info('Saved successful.');
            $appName =env('APP_NAME', 'GEC TV');
            $appUrl =env('APP_URL', 'tv.gecequip.org');
            $email = [
                'greeting' => 'Greeting '.$user->title." ".$user->firstName." ".$user->lastName.',',
                'body' => "You have successful updated your password on $appName , your GEC unique USER ID: ".$user->username
                    ." password : ".$request->password,
                'thanks' => 'This is from '.$appUrl,
                'actionText' => 'Login',
                'actionURL' => url('/'),
                'id' => $user->memberid
            ];

            Notification::send($user, new EmailNotification($email));

            Log::info("You have successful updated account, Gec User ID is " . $user->username);
            return redirect()->route("sign-in")->with("success", "Password Successful updated, Check your email for a new password.\nYour User ID is " . $user->username);
        } else {

            return back()->with("danger", " Oops!, could not update password");
        }


        //redirect: sign up or proceed to other details

    }

    public function passwordReset()
    {
        return view("reset-password");
    }




}
