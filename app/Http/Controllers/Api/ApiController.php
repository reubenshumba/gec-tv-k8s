<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Meeting;
use Illuminate\Http\JsonResponse;


class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }



    public function fetchMeetingsWithAuthorities($id = null): JsonResponse
    {

        $authorityIds = [16, 4];

        // If the provided ID is not in the list of authorized IDs, return not found
        if ($id === null || !in_array($id, $authorityIds)) {
            return response()->json(['message' => 'ID not found'], 400);
        }

        // Fetch meetings with specified authority ID
        $meetings = Meeting::with(['authorities' => function ($query) use ($id) {
            $query->where('authorities.authorityid', $id); // Use $id here
        }])->get();

        return response()->json($meetings);
    }


}
