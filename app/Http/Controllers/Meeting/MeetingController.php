<?php

namespace App\Http\Controllers\Meeting;

use App\Http\Controllers\Controller;
use App\Models\Meeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MeetingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index($id)
    {
        $meeting = Meeting::find($id);
        if( !isset($meeting) || $meeting == null ){
            return back()->with("danger","Oops! please try again.");
        }

        return view('stream.index',["meeting"=>$meeting,"user"=>Auth::user()]);
    }
}
