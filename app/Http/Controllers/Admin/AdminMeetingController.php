<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Authority;
use App\Models\Meeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdminMeetingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if( ! (Auth::user()->authority_authorityid == 2 || Auth::user()->authority_authorityid == 1 ) ){
            return view('stream.404');
        }
            return view('admin.meeting.index',
                ["meetings" => DB::table('meetings')->orderby("meetingid","DESC")->simplePaginate(5)]);


    }


    public function create()
    {
        if( ! (Auth::user()->authority_authorityid == 2 || Auth::user()->authority_authorityid == 1 ) ){
            return view('stream.404');
        }
        $authorities = Authority::select()->get("*");
        return view('admin.meeting.create', ["authorities" => $authorities]);
    }

    public function store(Request $request)
    {
        try{
            if( ! (Auth::user()->authority_authorityid == 2 || Auth::user()->authority_authorityid == 1 ) ){
                return view('stream.404');
            }

            $this->validate($request, [
                'active' => "required|boolean|in:1,0",
                'description' => "max:255",
                'enable' => "required|boolean|in:1,0",
                'host' => "required|min:3|max:100",
                'meeting_url' => "required",
                'name' => "required|min:3|max:100",
                'authority' => "required"
            ]);

            $embed_supported_type = "";
            if (str_starts_with(strtolower($request->meeting_url), strtolower('http'))) {
                $embed_supported_type = "URL";
            } elseif (str_starts_with(strtolower($request->meeting_url), strtolower('<iframe'))) {
                $embed_supported_type = "IFRAME";
            }elseif(strpos(strtolower($request->meeting_url),strtolower('<iframe')) !== false){
                $embed_supported_type = "IFRAME";
            }

            $meeting = new Meeting([
                'active' => (boolean)$request->active,
                'description' => $request->description,
                'embed_supported_type' => $embed_supported_type,
                'enable' => (boolean)$request->enable,
                'host' => $request->host,
                'meeting_chat_url' => $request->meeting_chat_url,
                'meeting_url' => $request->meeting_url,
                'name' => $request->name,
                'date_created' => date('Y-m-d H:i:s'),
                'date_updated' => date('Y-m-d H:i:s')
            ]);
            $meeting->save();
            $meeting->authorities()->attach($request->authority );
            $meeting->authorities()->attach(1);

            return redirect()->route("admin-meetings")->with("success", "You have successful added a meeting : " . $request->name);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return back()->withInput()->with("error", "An error occurred while processing your request. Please try again.");
        }

    }

    public function edit($id)
    {
        if( ! (Auth::user()->authority_authorityid == 2 || Auth::user()->authority_authorityid == 1 ) ){
            return view('stream.404');
        }

        $meeting = new Meeting();
        $meeting = $meeting->findorfail($id);
        $authorities = Authority::select()->get("*");;
        return view('admin.meeting.edit', ["authorities" => $authorities, "meeting" => $meeting]);
    }


    public function update(Request $request, $id)
    {
        if( Auth::user()->authority_authorityid != 1  ){
            return back()->with('danger'," You do not have enough privileges.");
        }
        $this->validate($request, [
            'meetingid'=>"required",
            'active' => "required|boolean|in:1,0",
            'description' => "max:255",
            'enable' => "required|boolean|in:1,0",
            'host' => "required|min:3|max:100",
            'meeting_url' => "required",
            'name' => "required|min:3|max:100",
            'authority' => "required"
        ]);

        if($id != $request->meetingid){
            return back()->with('danger'," Error, Please edit the right meeting.");
        }

        $embed_supported_type = "";
        if (str_starts_with(strtolower($request->meeting_url), strtolower('http'))) {
            $embed_supported_type = "URL";
        } elseif (str_starts_with(strtolower($request->meeting_url), strtolower('<iframe'))) {
            $embed_supported_type = "IFRAME";
        }elseif(strpos(strtolower($request->meeting_url),strtolower('<iframe')) !== false){
            $embed_supported_type = "IFRAME";
        }

        $meeting = new Meeting();
        $meeting = $meeting->findorfail($id);
        $meeting->active = (boolean)$request->active;
        $meeting->description = $request->description;
        $meeting->embed_supported_type = $embed_supported_type;
        $meeting->enable = (boolean)$request->enable;
        $meeting->host = $request->host;
        $meeting->meeting_chat_url = $request->meeting_chat_url;
        $meeting->meeting_url = $request->meeting_url;
        $meeting->name = $request->name;
        $meeting->date_updated = date('Y-m-d H:i:s');
        $meeting->save();

        $authority = new Authority();
        $authorities = $authority->find($request->authority);
        $meeting->authorities()->detach();
        $meeting->authorities()->attach($authorities);
        $meeting->authorities()->attach(1);

        return redirect()->route("admin-meetings")->with("success", "You have successful updated a meeting : " . $request->name);
    }

    public function delete($id)
    {
        if( Auth::user()->authority_authorityid != 1  ){
            return back()->with('danger'," You do not have enough privileges.");
        }
        $meeting = new Meeting();
        $meeting = $meeting->findorfail($id);
        $meeting->authorities()->detach();
        $meeting->delete();
        return redirect()->route("admin-meetings")->with("success",
            $meeting->name.  " Has been successful removed");

    }
}
