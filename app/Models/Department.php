<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $primaryKey = 'departmentid';

   protected $fillable = [
        'departmentid',
        'active',
        'department_name',
        'description',
        'date_created',
        'date_updated',
    ];
}
