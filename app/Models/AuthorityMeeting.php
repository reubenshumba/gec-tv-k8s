<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AuthorityMeeting extends Pivot
{
    use HasFactory;
    public $timestamps = false;
    protected $table ="authority_meeting";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'authorityid',
         'meetingid'
    ];
}
