<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Member extends Authenticatable
{
    public $timestamps = false;
    protected $primaryKey = 'memberid';
    use HasFactory;
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "memberid",
        'username',
        'email',
        'password',
        'first_name',
        'last_name',
        'active',
        'date_created',
        'date_updated',
        'enable',
        'foundation_school',
        'gender',
        'title',
        'authority_authorityid',
        'branchid',
        "updated_at",
        "created_at"
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];


    public function authorities()
    {
        return $this->belongsToMany(Authority::class, 'authority_member', "memberid", "authorityid", "memberid", "authorityid", "authorities");
    }
}
