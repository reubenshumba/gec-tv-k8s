<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $primaryKey = 'meetingid';


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'meetingid',
        'active',
        'date_created',
        'date_updated',
        'description',
        'embed_supported_type',
        'enable',
        'host',
        'meeting_chat_url',
        'meeting_url',
        'name'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authorities()
    {
       // return $this->belongsToMany(Authority::class,"authority_meeting");//->using(AuthorityMeeting::class);
        return $this->belongsToMany(Authority::class,
            "authority_meeting",
            "meetingid",
            "authorityid",
            "meetingid",
            "authorityid",
        "authorities");//->using(AuthorityMeeting::class);


    }
}
