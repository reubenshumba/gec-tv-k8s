<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $primaryKey = 'authorityid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'authorityid',
        'authority',
        'authority_name',
        'short_description',
    ];



    /**
     * The users that belong to the role.
     */
    public function meetings()
    {
        return $this->belongsToMany(Meeting::class,"authority_meeting","authorityid",
            "meetingid","authorityid","meetingid");//->using(AuthorityMeeting::class);

    }
}
