<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;

    protected $table = 'guest';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'fullname',
        'email',
        'participant',
        'meetingid',
        'phoneNumber',
         'city',       // Added the new 'city' field to allow mass assignment
        'country',    // Added the new 'country' field to allow mass assignment
        'member'      // Added the new 'member' field to allow mass assignment
        // Add any other fields you want to allow mass assignment for

        // Add any other fields you want to allow mass assignment for
    ];
}
