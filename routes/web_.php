<?php

use Illuminate\Support\Facades\Route;


use App\Http\Controllers\Admin\AdminMeetingController;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\Meeting\MeetingController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Auth::routes();
//
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/',[LoginRegisterController::class,"index"])->name("sign-in");
Route::post('/',[LoginRegisterController::class,"login"]);

Route::get('/sign-up',[LoginRegisterController::class,"signUp"])->name("sign-up");
Route::post('/sign-up',[LoginRegisterController::class,"saveSignUp"]);

Route::get('/register/{id}',[LoginRegisterController::class,"register"])->name("register" );
Route::post('/register/{id}',[LoginRegisterController::class,"saveRegister"]);
Route::get('/sendEmail', [LoginRegisterController::class,'send'])->name('email.send');


Auth::routes(['register' => false]);
Route::get('/meeting/{id}',[MeetingController::class,"index"])->name("stream");

Route::get('/admin/meetings/',[AdminMeetingController::class,"index"])->name("admin-meetings");

Route::get('/admin/meetings/create',[AdminMeetingController::class,"create"])->name("admin-meetings-create");;
Route::post('/admin/meetings/create',[AdminMeetingController::class,"store"]);

Route::get('/admin/meetings/{id}/edit',[AdminMeetingController::class,"edit"])->name("admin-meetings-edit");
Route::post('/admin/meetings/{id}/edit',[AdminMeetingController::class,"update"]);

Route::post('/admin/meetings/{id}/delete',[AdminMeetingController::class,"delete"])->name("admin-meetings-delete");;

