<?php

use Illuminate\Support\Facades\Route;


use App\Http\Controllers\Admin\AdminMeetingController;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\Meeting\MeetingController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/logout', [LoginRegisterController::class,"logout"])->name("logout");
Route::get('/tc', [LoginRegisterController::class, "tc"])->name("tc-sign-in");
Route::get('/cc', [LoginRegisterController::class, "cc"])->name("cc-sign-in");
Route::get('/fl', [LoginRegisterController::class, "fl"])->name("fl-sign-in");
Route::get('/fsl', [LoginRegisterController::class, "fsl"])->name("fsl-sign-in");
Route::get('/cm', [LoginRegisterController::class, "cm"])->name("cm-sign-in");
Route::get('/clm', [LoginRegisterController::class, "clm"])->name("clm-sign-in");
Route::get('/',[LoginRegisterController::class,"index"])->name("sign-in");
Route::post('/',[LoginRegisterController::class,"login"]);

Route::get('/reset-my-password',[LoginRegisterController::class,"passwordReset"])->name("reset-my-password");
Route::post('/reset-my-password',[LoginRegisterController::class,"updatePassword"]);

Route::get('/sign-up',[LoginRegisterController::class,"signUp"])->name("sign-up");
Route::post('/sign-up',[LoginRegisterController::class,"saveSignUp"]);


Route::get('/register/{id}',[LoginRegisterController::class,"register"])->name("register" );
Route::post('/register/{id}',[LoginRegisterController::class,"saveRegister"]);
Route::get('/sendEmail', [LoginRegisterController::class,'send'])->name('email.send');


Auth::routes(['register' => false]);
Route::get('/meeting/{id}',[MeetingController::class,"index"])->name("stream");

Route::get('/admin/meetings/',[AdminMeetingController::class,"index"])->name("admin-meetings");

Route::get('/admin/meetings/create',[AdminMeetingController::class,"create"])->name("admin-meetings-create");
Route::post('/admin/meetings/store',[AdminMeetingController::class,"store"])->name("admin-meetings-store");

Route::get('/admin/meetings/{id}/edit',[AdminMeetingController::class,"edit"])->name("admin-meetings-edit");
Route::post('/admin/meetings/{id}/update',[AdminMeetingController::class,"update"])->name("admin-meetings-update");

Route::post('/admin/meetings/delete/{id}',[AdminMeetingController::class,"delete"])->name("admin-meetings-delete");

