<?php if($notification = Session::get('success')): ?>
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($notification); ?></strong>
    </div>
<?php endif; ?>


<?php if($notification = Session::get('error')): ?>
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($notification); ?></strong>
    </div>
<?php endif; ?>

<?php if($notification = Session::get('danger')): ?>
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($notification); ?></strong>
    </div>
<?php endif; ?>


<?php if($notification = Session::get('warning')): ?>
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($notification); ?></strong>
    </div>
<?php endif; ?>


<?php if($notification = Session::get('info')): ?>
    <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($notification); ?></strong>
    </div>
<?php endif; ?>


<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        Please check the form under for errors
    </div>
<?php endif; ?>
<?php /**PATH /home/gecequip/GecTV/resources/views/alert/alert-notification.blade.php ENDPATH**/ ?>