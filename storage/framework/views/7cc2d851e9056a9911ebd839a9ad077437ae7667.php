<?php $__env->startSection("content"); ?>
    <div class="justify-content-center ">
        <form action="<?php echo e(route('register',['id'=>$member->memberid])); ?>" method="post" class="font-weight-bold">
            <?php echo csrf_field(); ?>

            <div class="row ">

                <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                    <div class="row justify-content-center card-body ">
                        <div class="col-md-8">
                            <div class="mb-4 text-center">
                                <img src="<?php echo e(asset('images/GEC-logo.png')); ?>" width="90"/>
                                <h3 class="mt-1"> Almost done</h3>
                                <p class="mb-4 h5">Fill in your Ministry Information</p>
                            </div>
                            <input class="form-control" placeholder="memberID" required
                                   hidden="hidden" name="memberid"
                                   type="hidden" value="<?php echo e($member->memberid); ?>">

                            <div class="form-group  mb-3">

                                <label for="foundationSchool">Have you completed foundation School
                                </label>
                                <select id="foundationSchool" name="foundationSchool"
                                        class="form-control  <?php $__errorArgs = ['foundationSchool'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">

                                    <option value="Yes">Yes, I have</option>
                                    <option value="Not">No, I have not</option>
                                    <option value="Studen">No, But enrolled</option>
                                </select>

                                <?php $__errorArgs = ['foundationSchool'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            </div>

                            <div class="form-group  mb-3">

                                <label for="branch">Select Branch </label>
                                <select id="branch" name="branch"
                                        class="form-control  <?php $__errorArgs = ['branch'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">

                                    <option value="-1">. . . </option>
                                   <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($branch->branchid); ?>"><?php echo e($branch->branch_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>

                                <?php $__errorArgs = ['branch'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            </div>

                            <div class="form-group  mb-3">

                                <label for="department">Select Department
                                </label>
                                <select id="department" name="department"
                                        class="form-control  <?php $__errorArgs = ['department'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">

                                    <option value="-1">. . .</option>
                                    <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($department->departmentid); ?>"><?php echo e($department->department_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>

                                <?php $__errorArgs = ['department'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            </div>

                            <input type="submit" value="Save" class="btn btn-block btn-outline-info form-control shadow">

                        </div>
                    </div>

                </div>

                

                <div class="col-md-6 shadow-lg justify-content-center p-5">
                    <div class="shadow-lg">
                        <img src="<?php echo e(asset('/images/bg.jpg')); ?>" alt="Image" class="img-fluid" width="800px">
                    </div>
                </div>

                
            </div>


        </form>
    </div>
<?php $__env->stopSection(); ?>





<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/gecequip/GecTV/resources/views/register.blade.php ENDPATH**/ ?>