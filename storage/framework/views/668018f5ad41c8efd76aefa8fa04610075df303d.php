

<?php $__env->startSection("content"); ?>
    <div class="justify-content-center ">
        <form action="<?php echo e(route('sign-up')); ?>" method="post" class="font-weight-bold">
            <?php echo csrf_field(); ?>

            <div class="row ">

                <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                    <div class="row justify-content-center card-body ">
                        <div class="col-md-8">
                            <div class="mb-4 text-center">
                                <img src="<?php echo e(asset('images/GEC-logo.png')); ?>" width="90"/>
                                <h3 class="mt-1"></h3>
                                <p class="mb-4 h5">Sign Up </p>
                            </div>

                            <div class="form-group  mb-3 ">
                                <label for="title">Select Title
                                </label>
                                <select id="title" name="title"  class="form-control  <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                                    <option value="Pastor">Pastor</option>
                                    <option value="Reverend">Reverend</option>
                                    <option value="H.E">H.E</option>
                                    <option value="Prophet">Prophet</option>
                                    <option value="Apostle">Apostle</option>
                                    <option value="Evangelist">Evangelist</option>
                                    <option value="Sir">Sir</option>
                                    <option value="Madam">Madam</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Deacon">Deacon</option>
                                    <option value="Deaconess">Deaconess</option>
                                    <option value="Director">Director</option>
                                </select>

                                <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            </div>

                            <div class="form-group first mb-4">
                                <label for="firstName">First Name</label>
                                <input type="text" name="firstName" value="<?php echo e(old('firstName')); ?>" required class="form-control  <?php $__errorArgs = ['firstName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="firstName">
                                <?php $__errorArgs = ['firstName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>

                            <div class="form-group first mb-4">
                                <label for="lastName">Last Name</label>
                                <input type="text" name="lastName" value="<?php echo e(old('lastName')); ?>"  required class="form-control  <?php $__errorArgs = ['lastName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="lastName">
                                <?php $__errorArgs = ['lastName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>


                            <div class="form-group last mb-4">
                                <label for="password">Password</label>
                                <input autocomplete="current-password" type="password" name="password" required
                                       class="form-control  <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="password">
                                <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                            <div class="form-group last mb-4">
                                <label for="confirmPassword">Confirm Password</label>
                                <input autocomplete="current-password" type="password" name="password_confirmation"
                                       required class="form-control  <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                       id="confirmPassword">

                            </div>

                        </div>
                    </div>

                </div>

                

                <div class="col-md-6 shadow-lg justify-content-center p-5">
                    <p class="mb-4 h5">Other details </p>

                    <div class="form-group first mb-4">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="<?php echo e(old('email')); ?>"  class="form-control  <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="email">

                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    <div class="form-group mb-2">
                        <label class="text-left" for="gender">Select Gender</label>
                        <select class="form-control <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="gender" name="gender">
                            <option value="-1"> . . .</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                        <?php $__errorArgs = ['gender'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>

                    <div class="form-group  mb-3">

                        <label for="member">Are you a member of Gospel Envoy's church ? </label>
                        <select id="member" name="member"
                                class="form-control  <?php $__errorArgs = ['member'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">

                            <option value="false">No I'm not </option>
                            <option value="true">Yes I'm</option>
                        </select>

                        <?php $__errorArgs = ['member'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                    </div>




                    <div class="d-flex justify-content-evenly ">

                        <div class="text-info">
                            <p class="checkbox-wrap checkbox-primary text-left  text-muted "
                               style="font-size: 10px; letter-spacing:  0.0625em; ">
                                By signing up, you confirm that you accept not share your login details with a third
                                party and that the ministry retains full and exclusive copyright ownership of all
                                segments of this service hence no segment of the broadcast should be recorded, copied,
                                re-transmitted in any form or by any means, stored in any form of electronic retrieval
                                system and used for any other purpose than that which it was intended.

                            </p>
                        </div>
                    </div>

                    <input type="submit" value="Sign Up" class="btn btn-block btn-outline-info form-control shadow ">

                    <span class="d-block text-center my-4 text-muted">&mdash;  or &mdash;</span>
                    <div class="mb-2">
                        <a href="<?php echo e(route('sign-in')); ?>" class="text-dark h4 mr-3 shadow-lg  p-2">
                            <span class="fa fa-sign-in mr-3  ">login</span>
                        </a>
                    </div>

                </div>

                
            </div>


        </form>
    </div>
<?php $__env->stopSection(); ?>





<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\git\gec-tv-k8s\resources\views/sign-up.blade.php ENDPATH**/ ?>