
<?php $__env->startSection("content"); ?>
    <div class="row  text-center h-100 justify-content-center align-items-center">

        <div class="col-md-12 col-lg-12 m-5">
            <div class=" text-center text-info">
                <h2 class="heading-section text-left pt-10">GEC LIVE | ADMIN PORTAL</h2>
            </div>
            <div class=" p-0 ">
                <div><h5 class="mb-4 text-text text-info">Meetings</h5></div>
                <div class="social d-flex text-center">
                    <a class="px-2 py-2 mr-md-1 rounded btn btn-outline-info"
                       href="<?php echo e(route("admin-meetings-create")); ?>"><span
                            class="ion-logo-facebook mr-2"></span>Add Meeting</a>

                    <a href="<?php echo e(route('logout')); ?>"
                       onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                    <span class="px-2 py-2 mr-md-1 rounded btn btn-outline-danger"> Logout <span>
                    </a>

                    <form id="frm-logout" action="<?php echo e(route('logout')); ?>" method="POST"
                          style="display: none;">
                        <?php echo e(csrf_field()); ?>

                    </form>
                </div>
                <div class="table-responsive">
                    <?php echo e($meetings->links()); ?>

                    <table class="table table-hover">
                        <thead>
                        <tr class="text-info text-left">
                            <th scope="col">#</th>
                            <th scope="col">ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Host</th>
                            <th scope="col">video information</th>
                            <th scope="col">Access Enabled</th>
                            <th scope="col">Visibility</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $meetings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="text-left">
                                <th scope="row"><?php echo e($key+1); ?></th>
                                <th scope="row"><?php echo e($value->meetingid); ?></th>
                                <td><?php echo e($value->name); ?></td>
                                <td><?php echo e($value->host); ?></td>
                                <td class="">

                                    <button class="btn btn-outline-info btn-sm btn-sm"
                                            data-toggle="modal"
                                            data-target="<?php echo e('#'.$value->meetingid); ?>"
                                            type="button">
                                        View
                                    </button>

                                    <!-- Modal -->
                                    <div aria-hidden="true" class="modal fade" role="dialog"
                                         tabindex="-1"
                                         aria-labelledby="<?php echo e($value->meetingid.'-'.$value->meetingid); ?>"
                                         id="<?php echo e($value->meetingid); ?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header text-info">
                                                    <h5 class="modal-title"
                                                        id="<?php echo e($value->meetingid."-".$value->meetingid); ?>">
                                                        <?php echo e($value->name." with ".$value->host); ?></h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal"
                                                            type="button">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <?php if($value->embed_supported_type =='URL'): ?>

                                                        <div
                                                            style="position:relative;padding-bottom:56.25%;overflow:hidden;height:0;max-width:100%;">
                                                            <iframe id="90b03aa3-c92d-8017-6899-6b0c3575c7c0"
                                                                    src="<?php echo e(html_entity_decode($value->meeting_url)); ?>"
                                                                    width="100%" height="100%" frameborder="0"
                                                                    scrolling="no" allow="autoplay;encrypted-media"
                                                                    allowfullscreen webkitallowfullscreen
                                                                    mozallowfullscreen oallowfullscreen
                                                                    msallowfullscreen
                                                                    style="position:absolute;top:0;left:0;"></iframe>
                                                        </div>

                                                    <?php endif; ?>

                                                    <?php if($value->embed_supported_type =='IFRAME'): ?>
                                                        <?php echo e(html_entity_decode($value->meeting_url)); ?>

                                                    <?php endif; ?>

                                                </div>
                                                <div class="modal-footer">

                                                    <p class="text-left "><?php echo e($value->description); ?></p>
                                                    <button class="btn btn-secondary" data-dismiss="modal"
                                                            type="button">
                                                        Close
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                                <td>
                                    <?php if($value->enable): ?>
                                        <span class="text-success"><?php echo e('Visible to Audience'); ?></span>
                                    <?php else: ?>
                                        <span class="text-warning"><?php echo e('Not Visible'); ?></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if($value->active): ?>
                                        <span class="text-success"><?php echo e('Published'); ?></span>
                                    <?php else: ?>
                                        <span class="text-warning"><?php echo e('Saved but not Published'); ?></span>
                                    <?php endif; ?>
                                </td>
                                <td><a class="btn btn-outline-success btn-sm"
                                       href="<?php echo e(route("admin-meetings-edit",['id'=>$value->meetingid])); ?>"> Edit</a>

                                    <button class="btn btn-outline-danger btn-sm btn-sm"
                                            data-toggle="modal"
                                            data-target="<?php echo e('#delete'.$value->meetingid); ?>"
                                            type="button">
                                        Delete
                                    </button>

                                    <!-- Modal -->
                                    <div aria-hidden="true" class="modal fade" role="dialog"
                                         tabindex="-1"
                                         aria-labelledby="<?php echo e('delete'.$value->meetingid.'-'.$value->meetingid); ?>"
                                         id="<?php echo e('delete'.$value->meetingid); ?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header text-danger">
                                                    <h5 class="modal-title"
                                                        id="<?php echo e('delete'.$value->meetingid.'-'.$value->meetingid); ?>"
                                                    >Confirmed Deletion</h5>
                                                    <button aria-label="Close" class="close" data-dismiss="modal"
                                                            type="button">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="text-danger">Are you sure you want to delete
                                                    <h5><?php echo e($value->name); ?> </h5></p>
                                                </div>
                                                <form  action="<?php echo e(route("admin-meetings-delete",['id'=>$value->meetingid])); ?>" method="post"
                                                    >
                                                    <?php echo csrf_field(); ?>
                                                    <button class="btn btn-outline-danger" type="submit">
                                                        Confirm Delete
                                                    </button>
                                                </form>
                                                <div class="modal-footer">

                                                    <p class="text-left "><?php echo e($value->description); ?></p>
                                                    <button class="btn btn-secondary" data-dismiss="modal"
                                                            type="button">
                                                        Close
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <?php echo e($meetings->links()); ?>

                </div>

                <div class="social d-flex text-center">
                    <a class="px-2 py-2 mr-md-1 rounded btn btn-outline-info"
                       href="<?php echo e(route("admin-meetings-create")); ?>"><span
                            class="ion-logo-facebook mr-2"></span>Add Meeting</a>

                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/gecequip/GecTV/resources/views/admin/meeting/index.blade.php ENDPATH**/ ?>