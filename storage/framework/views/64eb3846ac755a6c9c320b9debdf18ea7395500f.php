

<?php $__env->startSection("content"); ?>
    <div class="justify-content-center ">
        <div class="row ">

            <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                <div class="row justify-content-center card-body ">
                    <div class="col-md-8">
                        <div class="mb-4 text-center">
                            <img src="<?php echo e(asset('images/GEC-logo.png')); ?>" width="90"/>

                            <p class="mb-4 h5">Sign In </p>
                        </div>
                        <form action="<?php echo e(route('sign-in')); ?>" method="post" class="font-weight-bold" >
                            <?php echo csrf_field(); ?>
                            <div class="form-group first mb-4">
                                <label for="username">GEC User ID or Email</label>
                                <input type="text" required name="username"  value="<?php echo e(old('username')); ?>"
                                       class="form-control <?php $__errorArgs = ['username'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="username">

                                <?php $__errorArgs = ['username'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                            <div class="form-group last mb-4">
                                <label for="password">Password</label>
                                <input type="password" required  name="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="password">
                                <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                            <div class="form-group  mb-3">
                                <label for="meeting">Select Service
                                </label>
                                <select id="meeting" required name="meeting" class="form-control <?php $__errorArgs = ['meeting'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                                    <?php if(count($meetings) > 1): ?>
                                        <option value="-1">Select Meeting</option>
                                    <?php endif; ?>
                                    <?php $__currentLoopData = $meetings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $meeting): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($meeting->meetingid); ?>" ><?php echo e($meeting->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                                <?php $__errorArgs = ['meeting'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                            </div>

                            <div class="d-flex mb-5 align-items-center">
                                <label class="control control--checkbox mb-0"><span class="caption">Remember me</span>
                                    <input type="checkbox" name="remember" checked="checked"/>
                                    <div class="control__indicator"></div>
                                </label>

                            </div>



                            <input type="submit" value="Log In" class="btn btn-block btn-outline-info form-control ">

                            <div class="form-group last mt-1">

                            </div>

                            <span class="d-block text-left my-4 text-muted">&mdash; Reset Password or Sign up &mdash;</span>

                            <div class="d-flex justify-content-evenly ">
                                <a href="<?php echo e(route('reset-my-password')); ?>" class="btn btn-outline-primary">
                                    <span class="fa fa-sign-in mr-3">  Reset Password</span>
                                </a>
                                <a href="<?php echo e(route('sign-up')); ?>" class="btn btn-outline-dark">
                                    <span class="fa fa-sign-in mr-3"> Sign Up</span>
                                </a>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-md-6 shadow-lg justify-content-center p-5">
                <img  src="<?php echo e(asset('images/bg.jpg')); ?>" alt="Image" class="img-fluid" width="800px">
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>




<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\git\gec-tv-k8s\resources\views/index.blade.php ENDPATH**/ ?>