

<?php $__env->startSection("content"); ?>
    <section class="container h-100 pb-4">


        <div class="">
            <?php if(!$meeting->enable): ?>
                <h3 class="text-warning">This meeting is not public to the viewers </h3>
            <?php elseif(!$meeting->active): ?>
                <h3 class="text-warning">This meeting is not enable for viewing </h3>
            <?php else: ?>

                <h4 class="text-left text-muted text-info"><span>  <?php echo e($meeting->name); ?></span></h4>

                <div class="row  text-center  ">

                    <div class="col-md-7 col-lg-8 col-sm-12 ">

                        <div class="shadow-lg p-2  ">
                            <!-- 21:9 aspect ratio -->
                            <?php if($meeting->embed_supported_type =='URL'): ?>

                                <div
                                    style="position:relative;padding-bottom:56.25%;overflow:hidden;height:0;max-width:100%;">
                                    <iframe id="90b03aa3-c92d-8017-6899-6b0c3575c7c0"
                                            src="<?php echo e(html_entity_decode($meeting->meeting_url)); ?>"
                                            width="100%" height="100%" frameborder="0"
                                            scrolling="no" allow="autoplay;encrypted-media"
                                            allowfullscreen webkitallowfullscreen
                                            mozallowfullscreen oallowfullscreen
                                            msallowfullscreen
                                            style="position:absolute;top:0;left:0;"></iframe>
                                </div>

                            <?php endif; ?>

                            <?php if($meeting->embed_supported_type =='IFRAME'): ?>
                            <div class="embed-responsive embed-responsive-21by9 shadow-lg">
                                <span class="embed-responsive-item">
                                   
                                <?php echo e(html_entity_decode($meeting->meeting_url)); ?>

                                
                                </span>
                            </div>

                            <?php endif; ?>
                            <br/>


                        </div>


                    </div>
                    <div class="col-md-5 col-lg-4 col-sm-12">
                        
                        <div class="social d-flex text-center">
                           
                            
                            <div class="embed-responsive embed-responsive-21by9 shadow-lg">
                                <span class="embed-responsive-item">
                                    <?php echo html_entity_decode($meeting->meeting_chat_url); ?>

                                </span>
                            </div>


                        </div>

                        <hr/>

                    </div>
                </div>

                <div class="row mt-5 ">
                    <div class="col-md-6 col-lg-6 text-center shadow p-3">
                        <h4 class="text-left text-muted">
                            <?php echo e($meeting->host); ?></h4>
                        <p class="text-left text-sm-start"><?php echo e($meeting->description); ?> </p>
                        <br/>
                    </div>
                    <div class="col-md-6 col-lg-6 p-2 ">

                        <?php if(auth()->guard()->check()): ?>


                            <div class="text-dark shadow text-left">
                                <div class="card">
                                    <div class="card-header text-left">
                                        <?php echo e(Auth::user()->username); ?>

                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title text-left"> <?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?></h5>
                                        <p class="card-text text-left"> <?php echo e(Auth::user()->email); ?></p>

                                        <hr/>

                                        <a href="<?php echo e(route('logout')); ?>"
                                           onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                    <span class="px-2 py-2 mr-md-1 rounded btn btn-outline-danger"> Logout <span>
                                        </a>

                                        <form id="frm-logout" action="<?php echo e(route('logout')); ?>" method="POST"
                                              style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </div>
                                </div>

                            </div>

                        <?php endif; ?>


                    </div>
                </div>

            <?php endif; ?>
        </div>

    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/gecequip/GecTV/resources/views/stream/index.blade.php ENDPATH**/ ?>