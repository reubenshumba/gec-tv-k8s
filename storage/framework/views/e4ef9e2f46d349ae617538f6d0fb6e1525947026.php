

<?php $__env->startSection("content"); ?>
    <section class="container h-100 text-info">
        <form class="signin-form text-left " action="<?php echo e(route("admin-meetings-edit",['id'=>$meeting->meetingid])); ?>" method="post">
            <?php echo csrf_field(); ?>
            <div class="h-20 justify-content-around align-items-center  ">
                <h2 class="heading-section text-left text-capitalize mt-5 "> Meeting</h2>
                <a class="btn btn-outline-info btn-sm" href="<?php echo e(route("admin-meetings")); ?>"
                ><?php echo e('< View All Meetings'); ?> </a>
                <h5 class="mb-4 text-center">Edit meeting meeting</h5>
            </div>

            <div class="row  text-left h-100 justify-content-center align-items-center font-weight-bold">
<input type="hidden" value="<?php echo e($meeting->meetingid); ?>" name="meetingid">
                <div class="col-md-6 col-lg-6  p-5">
                    <div class="login-wrap p-0 text-left">

                        <div class="form-group  <?php $__errorArgs = ['host'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3">
                            <label class="text-left" for="host">Speaker </label>
                            <input id="host" class="form-control" placeholder="Speaker e.g Pastor Choolwe"
                                   required type="text" name="host" value="<?php echo e(old("host",$meeting->host)); ?>">

                            <?php $__errorArgs = ['host'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3">
                            <label class="text-left" for="name">Tag Name </label>
                            <input id="name" class="form-control" placeholder="Title e.g Sunday Service"
                                   required name="name" type="text" value="<?php echo e(old('name',$meeting->name)); ?>">

                            <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3">
                            <label class="text-left" for="description">Short Description </label>
                            <input id="description" class="form-control" placeholder="Short Description "
                                   name="description" type="text" value="<?php echo e(old('description',$meeting->description)); ?>">
                            <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  <?php $__errorArgs = ['meeting_url'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3 ">
                            <label class="text-left" for="meeting_url">Enter URL or Embed Code </label>
                            <textarea id="meeting_url" class="form-control" placeholder="Url or Embed "
                                      required name="meeting_url" type="text" value="<?php echo e(old('meeting_url',$meeting->meeting_url)); ?>"
                            ><?php echo e(old('meeting_url',$meeting->meeting_url)); ?></textarea>
                            <?php $__errorArgs = ['meeting_url'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  mb-3">
                            <label class="text-left" for="chat">Chat Embed Code </label>
                            <textarea id="for" class="form-control" placeholder="Chat Embed Code "
                                      name="meeting_chat_url" type="text"
                                      value="<?php echo e(old('meeting_chat_url',$meeting->meeting_chat_url)); ?>"
                            ><?php echo e(old('meeting_chat_url',$meeting->meeting_chat_url)); ?></textarea>

                        </div>

                    </div>
                </div>
                <div class="col-md-6 text-left p-5 pt-0">
                    <div class="login-wrap p-0 text-left">
                        <div class="form-group  <?php $__errorArgs = ['enable'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3 ">
                            <label class="text-left" for="enabled">Make the link accessible to views </label>
                            <select class="form-control" id="enabled" name="enable">
                                <option value="1" <?php if($meeting->enable): ?> selected <?php endif; ?> >Enabled to Audience</option>
                                <option value="0" <?php if(!$meeting->enable): ?> selected <?php endif; ?> >Disable to Audience</option>
                            </select>
                            <?php $__errorArgs = ['enable'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  <?php $__errorArgs = ['active'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3 ">
                            <label class="text-left" for="active">Publish or Save</label>
                            <select class="form-control" id="active" name="active">
                                <option value="1" <?php if($meeting->active): ?> selected <?php endif; ?>>Public to Audience</option>
                                <option value="0"<?php if(!$meeting->active): ?> selected <?php endif; ?>>Save but dont publish to Audience</option>
                            </select>
                            <?php $__errorArgs = ['active'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  <?php $__errorArgs = ['authority'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>  mb-3 ">
                            <label class="text-left" for="authority">Select
                                Audience</label>
                            <select  class="form-control" id="authority"  multiple="multiple" name="authority[]">

                                <?php $__currentLoopData = $authorities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $authority): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($authority->authorityid); ?>" <?php $__currentLoopData = $meeting->authorities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $meetingAuthority): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($authority->authority_authorityid == $meetingAuthority ): ?> selected <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> ><?php echo e($authority->authority_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>

                            <?php $__errorArgs = ['authority'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="form-group  mt-5">
                            <button class="form-control btn btn-info submit px-3" type="submit">Update Meeting
                            </button>
                        </div>

                    </div>
                </div>


            </div>


        </form>
    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/gecequip/GecTV/resources/views/admin/meeting/edit.blade.php ENDPATH**/ ?>