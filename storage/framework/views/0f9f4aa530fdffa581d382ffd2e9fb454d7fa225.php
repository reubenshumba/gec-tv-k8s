
<?php $__env->startSection("content"); ?>
<section class="container h-100">
	<div class="row  text-center h-100 justify-content-center align-items-center">

		<div class="col-md-12 col-lg-12  p-5">
			<div class="text-center text-capitalize font-weight-bold">
				<h1 class="heading-section text-danger"> Forbidden | 403 </h1>
			</div>
			<div class="text-center">
				<!-- 21:9 aspect ratio -->
				<div class="text-danger font-weight-bold">
					<h2>You don't have access to this content</h2>
				</div>
			</div>

            <a href="<?php echo e(route('logout')); ?>"
               onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                    <span class="px-2 py-2 mr-md-1 rounded btn btn-outline-danger"> Logout </span>
            </a>

            <form id="frm-logout" action="<?php echo e(route('logout')); ?>" method="POST"
                  style="display: none;">
                <?php echo e(csrf_field()); ?>

            </form>

		</div>
	</div>
    
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/gecequip/GecTV/resources/views/stream/404.blade.php ENDPATH**/ ?>