

<?php $__env->startSection("content"); ?>
    <div class="justify-content-center ">
        <div class="row ">

            <div class="col-md-6 card rounded-end justify-content-center shadow-lg">
                <div class="row justify-content-center card-body ">
                    <div class="col-md-8">
                        <div class="mb-4 text-center">
                            <img src="<?php echo e(asset('images/GEC-logo.png')); ?>" width="90"/>

                            <p class="mb-4 h5">Reset Password </p>
                        </div>
                        <form action="<?php echo e(route('reset-my-password')); ?>" method="post" class="font-weight-bold" >
                            <?php echo csrf_field(); ?>
                            <div class="form-group first mb-4">
                                <label for="username">Account Email</label>
                                <input type="text" required name="email"  value="<?php echo e(old('email')); ?>"
                                       class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="email">

                                <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                            <div class="form-group last mb-4">
                                <label for="password">Password</label>
                                <input type="password" required  name="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="password">
                                <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong>  <?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                            <div class="form-group last mb-4">
                                <label for="confirmPassword">Confirm Password</label>
                                <input autocomplete="current-password" type="password" name="password_confirmation"
                                       required class="form-control  <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"
                                       id="confirmPassword">

                            </div>

                            <input type="submit" value="Update" class="btn btn-block btn-outline-info form-control ">

                            <div class="form-group last mt-1">
                            </div>

                            <span class="d-block text-left my-4 text-muted">&mdash; Sign In or Sign up &mdash;</span>

                            <div class="d-flex justify-content-evenly ">
                                <a href="<?php echo e(route('sign-in')); ?>" class="btn btn-outline-primary">
                                    <span class="fa fa-sign-in mr-3">  Sign In </span>
                                </a>
                                <a href="<?php echo e(route('sign-up')); ?>" class="btn btn-outline-dark">
                                    <span class="fa fa-sign-in mr-3"> Sign Up</span>
                                </a>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-md-6 shadow-lg justify-content-center p-5">
                <img  src="<?php echo e(asset('images/bg.jpg')); ?>" alt="Image" class="img-fluid" width="800px">
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>





<?php echo $__env->make("layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/gecequip/GecTV/resources/views/reset-password.blade.php ENDPATH**/ ?>